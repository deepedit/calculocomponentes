package centroenergia.cl.calculocomponentes;

/**
 * Simetrical Components and Park-Blundle and Clarke Phasorial Conversor labels
 * @author Frank Leanez
 */
public class AppLabels {

    /**
     * Function to retrieve all labels for the given operation (conversion of components) and language
     * @param opera Calculators constant for operations
     * @param language Calculators constants for supported languages
     * @return retrieve all labels for the given operation (conversion of components) and language
     */
    public static String[] getAppLabels (int opera, int language) {

        switch (opera){
            case Calculator.ABC_TO_ZERO12:
                switch (language) {
                    case Calculator.ENGLISH:
                        return getLabelsABCTo012English();
                    case Calculator.SPANISH:
                        return getLabelsABCTo012Spanish();
                    case Calculator.GERMAN:
                        return getLabelsABCTo012German();
                }
            case Calculator.ZERO12_TO_ABC:
                switch (language) {
                    case Calculator.ENGLISH:
                        return getLabels012ToABCEnglish();
                    case Calculator.SPANISH:
                        return getLabels012ToABCSpanish();
                    case Calculator.GERMAN:
                        return getLabels012ToABCGerman();
                }
            case Calculator.ABC_TO_PARK:
                switch (language) {
                    case Calculator.ENGLISH:
                        return getLabelsABCToParkEnglish();
                    case Calculator.SPANISH:
                        return getLabelsABCToParkSpanish();
                    case Calculator.GERMAN:
                        return getLabelsABCToParkGerman();
                }
            case Calculator.PARK_TO_ABC:
                switch (language) {
                    case Calculator.ENGLISH:
                        return getLabelsParkToABCEnglish();
                    case Calculator.SPANISH:
                        return getLabelsParkToABCSpanish();
                    case Calculator.GERMAN:
                        return getLabelsParkToABCGerman();
                }
            case Calculator.ABC_TO_CLARKE:
                switch (language) {
                    case Calculator.ENGLISH:
                        return getLabelsABCToClarkeEnglish();
                    case Calculator.SPANISH:
                        return getLabelsABCToClarkeSpanish();
                    case Calculator.GERMAN:
                        return getLabelsABCToClarkeGerman();
                }
            case Calculator.CLARKE_TO_ABC:
                switch (language) {
                    case Calculator.ENGLISH:
                        return getLabelsClarkeToABCEnglish();
                    case Calculator.SPANISH:
                        return getLabelsClarkeToABCSpanish();
                    case Calculator.GERMAN:
                        return getLabelsClarkeToABCGerman();
                }
        }
        return getLabelsABCTo012English();

    }

    /**
     * Function to retrieve all keys to access the labels used in this app
     * @return all keys to access the labels used in this app
     */
    public static String[] getKeys () {
        return new String[] {
                //0-1
                "title","input",
                //2-7
                "moda", "modb", "modc","anga","angb","angc",
                //8-13
                "Rea","Reb","Rec","Ima","Imb","Imc",
                //14-19
                "moda0","moda1","moda2","anga0","anga1","anga2",
                //20-25
                "Rea0","Rea1","Rea2","Ima0","Ima1","Ima2",
                //26
                "output",
                //27-32
                "actions","calculate","plot", "reset", "exit","help",
                //33-34
                "help_url","Theta"};
    }


    public static String[] getLabelsABCTo012English () {
        return new String[] {
                //0-1
                "Simetrycal Componets","Input Component: abc",
                //2-7
                "|a|", "|b|", "|c|","\u003Ca","\u003Cb","\u003Cc",
                //8-13
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //14-19
                "|a0|","|a1|","|a2|","\u003Ca0","\u003Ca1","\u003Ca2",
                //20-25
                "Re{a0}","Re{a1}","Re{a2}","Im{a0}","Im{a1}","Im{a2}",
                //26
                "Output Component: 012",
                //27-32
                "General Actions","Calculate","Graphic", "Reset", "Exit","Help",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html","Theta"};
    }
    public static String[] getLabelsABCTo012Spanish () {
        return new String[] {
                //0-1
                "Componentes Simétricas","Datos de las Fases abc:",
                //2-7
                "|a|", "|b|", "|c|","<a","<b","<c",
                //8-13
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //14-19
                "|a0|","|a1|","|a2|","<a0","<a1","<a2",
                //20-25
                "Re{a0}","Re{a1}","Re{a2}","Im{a0}","Im{a1}","Im{a2}",
                //26
                "Salidas Componentes: 012",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index_esp.html", "Theta"};
    }
    public static String[] getLabelsABCTo012German () {
        return new String[] {
                //0-1
                "Componentes Simétricas","Datos de las Fases abc:",
                //2-7
                "|a|", "|b|", "|c|","<a","<b","<c",
                //8-13
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //14-19
                "|a0|","|a1|","|a2|","<a0","<a1","<a2",
                //20-25
                "Re{a0}","Re{a1}","Re{a2}","Im{a0}","Im{a1}","Im{a2}",
                //26
                "Salidas Componentes: 012",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html", "Theta"};
    }
    public static String[] getLabels012ToABCEnglish () {
        return new String[] {
                //0-1
                "Symmetrical Components","Input Components: a0, a1, a2",
                //2-7
                "|a0|", "|a1|", "|a2|","\u003Ca0","\u003Ca1","\u003Ca2",
                //8-13
                "Re{a0}","Re{a1}","Re{a2}","Im{a0}","Im{a1}","Im{a2}",
                //14-19
                "|a|","|b|","|c|","\u003Ca","\u003Cb","\u003Cc",
                //20-25
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //26
                "Ouput Components: abc",
                //27-32
                "General Actions","Calculate","Graphic", "Reset", "Exit","Help",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html", "Theta"};
    }
    public static String[] getLabels012ToABCSpanish () {
        return new String[] {
                //0-1
                "Componentes Simétricas","Datos de las Componentes A0, A1, A2",
                //2-7
                "|a0|", "|a1|", "|a2|","<a0","<a1","<a2",
                //8-13
                "Re{a0}","Re{a1}","Re{a2}","Im{a0}","Im{a1}","Im{a2}",
                //14-19
                "|a|","|b|","|c|","<a","<b","<c",
                //20-25
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //26
                "Salidas Componentes ABC:",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index_esp.html", "Theta"};
    }

    public static String[] getLabels012ToABCGerman () {
        return new String[] {
                //0-1
                "Componentes Simétricas","Datos de las Componentes a0, a1, a2",
                //2-7
                "|a0|", "|a1|", "|a2|","<a0","<a1","<a2",
                //8-13
                "Re{a0}","Re{a1}","Re{a2}","Im{a0}","Im{a1}","Im{a2}",
                //14-19
                "|a|","|b|","|c|","<a","<b","<c",
                //20-25
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //26
                "Salidas Componentes ABC:",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html", "Theta"};
    }
    public static String[] getLabelsABCToParkEnglish () {
        return new String[] {
                //0-1
                "Park Blondel Componets","Input Components: abc",
                //2-7
                "|a|", "|b|", "|c|","\u003Ca","\u003Cb","\u003Cc",
                //8-13
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //14-19
                "|a0|","|ad|","|aq|","\u003Ca0","<ad","<aq",
                //20-25
                "Re{a0}","Re{ad}","Re{aq}","Im{a0}","Im{ad}","Im{aq}",
                //26
                "Output Component 0dq",
                //27-32
                "General Actions","Calculate","Graphic", "Reset", "Exit","Help",
                //33-34
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html", "Theta"};
    }
    public static String[] getLabelsABCToParkSpanish () {
        return new String[] {
                //0-1
                "Componentes de Park Blondel","Datos de las Fases ABC",
                //2-7
                "|a|", "|b|", "|c|","<a","<b","<c",
                //8-13
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //14-19
                "|a0|","|ad|","|aq|","<a0","<ad","<aq",
                //20-25
                "Re{a0}","Re{ad}","Re{aq}","Im{a0}","Im{ad}","Im{aq}",
                //26
                "Componentes 0dq",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33-34
                "http://flconsulting.cl/deepedit/calculocomponentes/index_esp.html", "Theta"};
    }
    public static String[] getLabelsABCToParkGerman () {
        return new String[] {
                //0-1
                "Componentes de Park Blondel","Datos de las Fases ABC",
                //2-7
                "|a|", "|b|", "|c|","<a","<b","<c",
                //8-13
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //14-19
                "|a0|","|ad|","|aq|","<a0","Angulo Ad1: ","<aq",
                //20-25
                "Re{a0}","Re{ad}","Re{aq}","Im{a0}","Im{ad}","Im{aq}",
                //26
                "Componentes 0dq",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33-34
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html" , "Theta"};
    }
    public static String[] getLabelsParkToABCEnglish () {
        return new String[] {
                //0-1
                "Park Blondel Componet","ABC Data Phase",
                //2-7
                "|a0|", "|ad|", "|aq|","\u003Ca0","<ad","<aq",
                //8-13
                "Re{a0}","Re{ad}","Re{aq}","Im{a0}","Im{ad}","Im{aq}",
                //14-19
                "|a|","|b|","|c|","\u003Ca","\u003Cb","\u003Cc",
                //20-25
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //26
                "ABC Component",
                //27-32
                "General Actions","Calculate","Graphic", "Reset", "Exit","Help",
                //33-34
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html", "Theta"};
    }
    public static String[] getLabelsParkToABCSpanish () {
        return new String[] {
                //0-1
                "Componentes de Park Blondel","Datos de las Componentes 0dq",
                //2-7
                "|a0|", "|ad|", "|aq|","<a0","<ad","<aq",
                //8-13
                "Re{a0}","Re{ad}","Re{aq}","Im{a0}","Im{ad}","Im{aq}",
                //14-19
                "|a|","|b|","|c|","<a","<b","<c",
                //20-25
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //26
                "Componentes ABC",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33-34
                "http://flconsulting.cl/deepedit/calculocomponentes/index_esp.html","Theta"};
    }
    public static String[] getLabelsParkToABCGerman () {
        return new String[] {
                //0-1
                "Componentes Simétricas","Datos de las Fases ABC",
                //2-7
                "|a|", "|b|", "|c|","<a","<b","<c",
                //8-13
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //14-19
                "|a0|","|ad|","|aq|","<a0","Angulo Ad1: ","<aq",
                //20-25
                "Re{a0}","Re{ad}","Re{aq}","Im{a0}","Im{ad}","Im{aq}",
                //26
                "Componentes 0dq fase A",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33-34
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html", "Theta"};
    }
    public static String[] getLabelsABCToClarkeEnglish () {
        return new String[] {
                //0-1
                "Simetrycal Componets","Input Components: abc",
                //2-7
                "|a|", "|b|", "|c|","\u003Ca","\u003Cb","\u003Cc",
                //8-13
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //14-19
                "|a0|","|a\u03B1|","|a\u03B2|","\u003Ca0","<a\u03B1","<a\u03B2",
                //20-25
                "Re{a0}","Re{a\u03B1}","Re{a\u03B2}","Im{a0}","Im{a\u03B1}","Im{a\u03B2}",
                //26
                "0 alpha betha Component",
                //27-32
                "General Actions","Calculate","Graphic", "Reset", "Exit","Help",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html", "Theta"};
    }
    public static String[] getLabelsABCToClarkeSpanish () {
        return new String[] {
                //0-1
                "Componentes Simétricas","Datos de las Fases ABC",
                //2-7
                "Modulo A:", "|b|", "|c|","<a","<b","<c",
                //8-13
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //14-19
                "|a0|","|a\u03B1|","|a\u03B2|","<a0","<a\u03B1","<a\u03B2",
                //20-25
                "Re{a0}","Re{a\u03B1}","Re{a\u03B2}","Im{a0}","Im{a\u03B1}","Im{a\u03B2}",
                //26
                "Componentes 0 alfa beta",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index_esp.html", "Theta"};
    }
    public static String[] getLabelsABCToClarkeGerman () {
        return new String[] {
                //0-1
                "Componentes Simétricas","Datos de las Fases ABC",
                //2-7
                "|a|", "|b|", "|c|","<a","<b","<c",
                //8-13
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //14-19
                "|a0|","|a\u03B1|","|a\u03B2|","<a0","<a\u03B1","<a\u03B2",
                //20-25
                "Re{a0}","Re{a\u03B1}","Re{a\u03B2}","Im{a\u03B2}","Im{a\u03B1}","Im{a\u03B2}",
                //26
                "Componentes 0 alfa beta",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html", "Theta"};
    }
    public static String[] getLabelsClarkeToABCEnglish () {
        return new String[] {
                //0-1
                "Symmetrical Components","Data Components A0, Aalfa, Abeta",
                //2-7
                "|a0|", "|a\u03B1|", "|a\u03B2|","\u003Ca0","<a\u03B1","<a\u03B2",
                //8-13
                "Re{a0}","Re{a\u03B1}","Re{a\u03B2}","Im{a0}","Im{a\u03B1}","Im{a\u03B2}",
                //14-19
                "|a|","|b|","|c|","\u003Ca","\u003Cb","\u003Cc",
                //20-25
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //26
                "Components ABC",
                //27-32
                "General Actions","Calculate","Graphic", "Reset", "Exit","Help",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html", "Theta"};
    }
    public static String[] getLabelsClarkeToABCSpanish () {
        return new String[] {
                //0-1
                "Componentes Simétricas","Datos de las Componentes A0, Aalfa, Abeta",
                //2-7
                "|a0|", "|a\u03B1|", "|a\u03B2|","<a0","<a\u03B1","<a\u03B2",
                //8-13
                "Re{a0}","Re{a\u03B1}","Re{a\u03B2}","Im{a0}","Im{a\u03B1}","Im{a\u03B2}",
                //14-19
                "|a|","|b|","|c|","<a","<b","<c",
                //20-25
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //26
                "Componentes ABC",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index_esp.html", "Theta"};
    }
    public static String[] getLabelsClarkeToABCGerman () {
        return new String[] {
//0-1
                "Componentes Simétricas","Datos de las Componentes A0, Aalfa, Abeta",
                //2-7
                "|a0|", "|a\u03B1|", "|a\u03B2|","<a0","<a\u03B1","<a\u03B2",
                //8-13
                "Re{a0}","Re{a\u03B1}","Re{a\u03B2}","Im{a0}","Im{a\u03B1}","Im{a\u03B2}",
                //14-19
                "|a|","|b|","|c|","<a","<b","<c",
                //20-25
                "Re{a}","Re{b}","Re{c}","Im{a}","Im{b}","Im{c}",
                //26
                "Componentes ABC",
                //27-32
                "Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
                //33
                "http://flconsulting.cl/deepedit/calculocomponentes/index.html", "Theta"};
    }


}
