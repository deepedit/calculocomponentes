package centroenergia.cl.calculocomponentes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b1 = findViewById(R.id.b1);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayABCTo012();
            }
        });
        Button b2 = findViewById(R.id.b2);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display012ToABC();
            }
        });
        Button b3 = findViewById(R.id.b3);
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayABCToClarke();
            }
        });
        Button b4 = findViewById(R.id.b4);
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayClarkeToABC();
            }
        });
        Button b5 = findViewById(R.id.b5);
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayABCToPark();
            }
        });
        Button b6 = findViewById(R.id.b6);
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayParkToABC();
            }
        });



    }

    private void displayABCTo012() {
        int language = getSelectedLanguage();
        Intent intent = new Intent(getApplicationContext(), Calculator.class);
        intent.putExtra("cl.centroenergia.calculocomponentes.Language", language);
        intent.putExtra("cl.centroenergia.calculocomponentes.Method", Calculator.ABC_TO_ZERO12);
        startActivity(intent);
    }

    private void display012ToABC() {
        int language = getSelectedLanguage();
        Intent intent = new Intent(getApplicationContext(), Calculator.class);
        intent.putExtra("cl.centroenergia.calculocomponentes.Language", language);
        intent.putExtra("cl.centroenergia.calculocomponentes.Method", Calculator.ZERO12_TO_ABC);
        startActivity(intent);
    }

    private void displayABCToPark() {
        int language = getSelectedLanguage();
        Intent intent = new Intent(getApplicationContext(), Calculator.class);
        intent.putExtra("cl.centroenergia.calculocomponentes.Language", language);
        intent.putExtra("cl.centroenergia.calculocomponentes.Method", Calculator.ABC_TO_PARK);
        startActivity(intent);
    }
    private void displayParkToABC() {
        int language = getSelectedLanguage();
        Intent intent = new Intent(getApplicationContext(), Calculator.class);
        intent.putExtra("cl.centroenergia.calculocomponentes.Language", language);
        intent.putExtra("cl.centroenergia.calculocomponentes.Method", Calculator.PARK_TO_ABC);
        startActivity(intent);
    }
    private void displayABCToClarke() {
        int language = getSelectedLanguage();
        Intent intent = new Intent(getApplicationContext(), Calculator.class);
        intent.putExtra("cl.centroenergia.calculocomponentes.Language", language);
        intent.putExtra("cl.centroenergia.calculocomponentes.Method", Calculator.ABC_TO_CLARKE);
        startActivity(intent);
    }
    private void displayClarkeToABC() {
        int language = getSelectedLanguage();
        Intent intent = new Intent(getApplicationContext(), Calculator.class);
        intent.putExtra("cl.centroenergia.calculocomponentes.Language", language);
        intent.putExtra("cl.centroenergia.calculocomponentes.Method", Calculator.CLARKE_TO_ABC);
        startActivity(intent);
    }

    private int getSelectedLanguage() {
        RadioButton bEnglish = findViewById(R.id.btnEnglish);
        RadioButton bSpanish = findViewById(R.id.btnSpanish);
        RadioButton bGerman = findViewById(R.id.btnGerman);

        if (bEnglish.isChecked()) {
            return Calculator.ENGLISH;
        } else if (bSpanish.isChecked()) {
            return Calculator.SPANISH;
        } else {
            return Calculator.GERMAN;
        }
    }
}
